<?php

/**
 * @file
 * Install file.
 */

use Drupal\views\Views;
use Drupal\Core\Database\Database;
use Drupal\solr_search_synonym\Entity\Synonym;

/**
 * @file
 * Contains solr_search_synonym.install.
 */

/**
 * Change length of the field 'word'.
 */
function solr_search_synonym_update_8001() {
  $spec = [
    'type' => 'varchar',
    'length' => 128,
    'not null' => FALSE,
  ];
  $schema = Database::getConnection()->schema();
  $schema->changeField('solr_search_synonym', 'word', 'word', $spec);
}

/**
 * Remove extra white spaces from synonyms.
 */
function solr_search_synonym_update_8002() {
  $sids = \Drupal::entityQuery('solr_search_synonym')
    ->condition('synonyms', '% %', 'LIKE')
    ->execute();

  foreach ($sids as $sid) {
    $synonym = Synonym::load($sid);
    $synonyms = explode(',', $synonym->getSynonyms());
    array_walk($synonyms, 'trim');
    $synonyms = implode(',', $synonyms);
    $synonym->setSynonyms($synonyms);
    $synonym->save();
  }
}

/**
 * Implements hook_uninstall().
 *
 * Drop table after module uninstall.
 */
function solr_search_synonym_uninstall() {
  \Drupal::database()->schema()->dropTable('solr_search_synonym');
}

/**
 * Add Import to SOLR Button.
 */
function solr_search_synonym_update_80011() {
  $view = Views::getView('solr_search_synonym');
  $options = [
    'id' => 'area',
    'table' => 'views',
    'field' => 'area',
    'relationship' => 'none',
    'group_type' => 'none',
    'admin_label' => '',
    'empty' => FALSE,
    'tokenize' => FALSE,
    'plugin_id' => 'text',
  ];
  $options['content']['value'] = '<a href="/admin/config/search/solr-search-synonyms/batch-export" class="button button--primary button--small">Import to Solr</a>';
  $options['content']['format'] = 'full_html';
  $view->setHandler('list', 'header', 'area', $options);
  $view->save(TRUE);
}
