<?php

namespace Drupal\solr_search_synonym\Form;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\solr_search_synonym\Import\Importer;
use Drupal\solr_search_synonym\Import\ImportPluginManager;
use Drupal\solr_search_synonym\ImportException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Class SynonymImportForm used for synonym import form.
 *
 * @package Drupal\solr_search_synonym\Form
 *
 * @ingroup solr_search_synonym
 */
class SynonymImportForm extends FormBase {

  /**
   * Import plugin manager.
   *
   * @var \Drupal\solr_search_synonym\Import\ImportPluginManager
   */
  protected $pluginManager;

  /**
   * An array containing available import plugins.
   *
   * @var array
   */
  protected $availablePlugins = [];

  /**
   * Constructs a SynonymImportForm object.
   *
   * @param \Drupal\solr_search_synonym\Import\ImportPluginManager $manager
   *   Import plugin manager.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translationInterface
   *   The string translation manager.
   */

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The string translation service.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $translationInterface;

  /**
   * Contruct function.
   */
  public function __construct(ImportPluginManager $manager, LanguageManagerInterface $language_manager, TranslationInterface $translationInterface) {
    $this->pluginManager = $manager;
    $this->languageManager = $language_manager;
    $this->stringTranslation = $translationInterface;

    foreach ($manager->getAvailableImportPlugins() as $id => $definition) {
      $this->availablePlugins[$id] = $manager->createInstance($id);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.solr_search_synonym.import'),
      $container->get('language_manager'),
      $container->get('string_translation'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'solr_search_synonym_import';
  }

  /**
   * The language manager service.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   */

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // File.
    $form['file_upload'] = [
      '#type' => 'file',
      '#title' => $this->t('File'),
      '#description' => $this->t('Select the import file.'),
      '#required' => FALSE,
    ];

    // Update.
    $form['update_existing'] = [
      '#type' => 'radios',
      '#title' => $this->t('Update existing'),
      '#description' => $this->t('What should happen with existing synonyms?'),
      '#options' => [
        'merge' => $this->t('Merge'),
        'overwrite' => $this->t('Overwrite'),
      ],
      '#default_value' => 'merge',
      '#required' => TRUE,
    ];

    // Synonym type.
    $form['synonym_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Type'),
      '#description' => $this->t('Which synonym type should the imported data be saved as?'),
      '#options' => [
        'synonym' => $this->t('Synonym'),
        'spelling_error' => $this->t('Spelling error'),
        'mixed' => $this->t('Mixed - Controlled by information in the source file'),
      ],
      '#default_value' => 'synonym',
      '#required' => TRUE,
    ];

    $message = $this->t('Notice: the source file must contain information per synonym about the synonym type. All synonyms without type information will be skipped during import!');
    $message = Markup::create('<div class="messages messages--warning">' . $message . '</div>');
    $form['synonym_type_notice'] = [
      '#type' => 'item',
      '#markup' => $message,
      '#states' => [
        'visible' => [
          ':radio[name="synonym_type"]' => ['value' => 'mixed'],
        ],
      ],
    ];

    // Activate.
    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Activate synonyms'),
      '#description' => $this->t('Mark import synonyms as active. Only active synonyms will be exported to the configured search backend.'),
      '#default_value' => TRUE,
      '#required' => TRUE,
    ];

    // Language code.
    $form['langcode'] = [
      '#type' => 'language_select',
      '#title' => $this->t('Language'),
      '#description' => $this->t('Which language should the imported data be saved as?'),
      '#default_value' => $this->languageManager->getCurrentLanguage()->getId(),
    ];

    // Import plugin configuration.
    $form['plugin'] = [
      '#type' => 'radios',
      '#title' => $this->t('Import format'),
      '#description' => $this->t('Choose the import format to use.'),
      '#options' => [],
      '#default_value' => key($this->availablePlugins),
      '#required' => TRUE,
    ];

    $form['plugin_settings'] = [
      '#tree' => TRUE,
    ];

    foreach ($this->availablePlugins as $id => $instance) {
      $definition = $instance->getPluginDefinition();
      $form['plugin']['#options'][$id] = $definition['label'];
      $form['plugin_settings'][$id] = [
        '#type' => 'details',
        '#title' => $this->t('@plugin plugin', ['@plugin' => $definition['label']]),
        '#open' => TRUE,
        '#tree' => TRUE,
        '#states' => [
          'visible' => [
            ':radio[name="plugin"]' => ['value' => $id],
          ],
        ],
      ];
      $form['plugin_settings'][$id] += $instance->buildConfigurationForm([], $form_state);
    }

    // Actions.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import file'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $values = $form_state->getValues();
    // Get plugin instance for active plugin.
    $instance_active = $this->getPluginInstance($values['plugin']);

    // Validate the uploaded file.
    $extensions = $instance_active->allowedExtensions();
    $validators = ['file_validate_extensions' => $extensions];

    $file = file_save_upload('file_upload', $validators, FALSE, 0, FileSystemInterface::EXISTS_RENAME);
    if (isset($file)) {
      if ($file) {
        $form_state->setValue('file_upload', $file);
      }
      else {
        $form_state->setErrorByName('file_upload', $this->t('The import file could not be uploaded.'));
      }
    }

    // Call the form validation handler for each of the plugins.
    foreach ($this->availablePlugins as $instance) {
      $instance->validateConfigurationForm($form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    try {
      // All values from the form.
      $values = $form_state->getValues();

      // Instance of active import plugin.
      $plugin_id = $values['plugin'];
      $instance = $this->getPluginInstance($plugin_id);

      // Parse file.
      $data = $instance->parseFile($values['file_upload'], (array) $values['plugin_settings'][$plugin_id]);

      // Import data.
      $importer = new Importer();
      $results = $importer->execute($data, $values);

      if (!empty($results['errors'])) {
        $count = count($results['errors']);
        $message = $this->stringTranslation->formatPlural($count,
          '@count synonym failed import.',
          '@count synonyms failed import.',
          ['@count' => $count]
        );
        $this->messenger()->addStatus($message);
      }
    }
    catch (ImportException $e) {
      $this->logger('solr_search_synonym')->error($this->t('Failed to import file due to "%error".', ['%error' => $e->getMessage()]));
      $this->messenger()->addStatus($this->t('Failed to import file due to "%error".', ['%error' => $e->getMessage()]));
    }
  }

  /**
   * Returns an import plugin instance for a given plugin id.
   *
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   *
   * @return \Drupal\solr_search_synonym\Import\ImportPluginInterface
   *   An import plugin instance.
   */
  public function getPluginInstance($plugin_id) {
    return $this->pluginManager->createInstance($plugin_id, []);
  }

}
