<?php

namespace Drupal\solr_search_synonym\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Synonym entities.
 *
 * @ingroup solr_search_synonym
 */
class SynonymDeleteForm extends ContentEntityDeleteForm {

}
