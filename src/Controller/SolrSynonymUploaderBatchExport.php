<?php

namespace Drupal\solr_search_synonym\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\solr_search_synonym\Export\ExportPluginManager;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * To change this license header, choose License Headers in Project Properties.
 *
 * To change this template file, choose Tools | Templates.
 *
 * And open the template in the editor.
 */
class SolrSynonymUploaderBatchExport extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Dependency injection of service.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The enity type manager used below as service.
   */

  /**
   * Dependency injection of sevice.
   *
   * @param \Drupal\solr_search_synonym\Export\ExportPluginManager $exportPluginManager
   *   The export plugin manager.
   */

  /**
   * The language manager service.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager service.
   */
  /**
   * Messanger service.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   * The messenger service.
   */

  /**
   * The config factory.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\solr_search_synonym\Export\ExportPluginManager
   */
  protected $exportPluginManager;

  /**
   * Contruct function for declaring services.
   */
  public function __construct(ExportPluginManager $exportPluginManager, LanguageManagerInterface $languageManager, ConfigFactoryInterface $config_factory, MessengerInterface $messenger, EntityTypeManagerInterface $entity_type_manager) {
    $this->exportPluginManager = $exportPluginManager;
    $this->languageManager = $languageManager;
    $this->configFactory = $config_factory;
    $this->messenger = $messenger;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.solr_search_synonym.export'),
      $container->get('language_manager'),
      $container->get('config.factory'),
      $container->get('messenger'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * Function index.
   */
  public function index() {

    $conf = $this->configFactory->getEditable('solr_search_synonym.settings')->get('cron');
    $plugin = 'solr_uploader';
    $options = [
      'langcode' => $this->languageManager->getCurrentLanguage()->getId(),
      'type' => $conf['type'],
      'filter' => $conf['filter'],
    ];
    $pluginManager = $this->exportPluginManager;
    $pluginManager->setPluginId($plugin);
    $pluginManager->setExportOptions($options);
    $synonyms = $pluginManager->getSynonymsData();
    $redirect_url = '/admin/config/search/solr-search-synonyms';
    if (!empty($synonyms)) {
      $batch = [
        'title' => $this->t('Import Data...'),
        'operations' => [],
        'init_message' => $this->t('Commencing'),
        'progress_message' => $this->t('Processing Synonyms'),
        'error_message' => $this->t('An error occurred during processing'),
        'finished' => '\Drupal\solr_search_synonym\Controller\SolrSynonymUploaderBatchExport::importDataFinished',
      ];

      foreach ($synonyms as $synonym) {
        $instance = $pluginManager->createInstance($plugin, []);
        $lines[] = $instance->generateLine($synonym->word, $synonym->synonyms, $synonym->type);
      }
      $batch['operations'][] = ['\Drupal\solr_search_synonym\Controller\SolrSynonymUploaderBatchExport::importData',
      [$lines],
      ];
      batch_set($batch);
      return batch_process($redirect_url);
    }
    else {
      $this->messenger->addStatus('No Synonyms found.');
      return new RedirectResponse($redirect_url);
    }

  }

  /**
   * Static function import data.
   */
  public static function importData($lines, &$context) {
    $container = \Drupal::getContainer();
    $pluginManager = $container->get('plugin.manager.solr_search_synonym.export');
    $entityTypeManager = $container->get('entity_type.manager');
    $plugin = 'solr_uploader';
    $instance = $pluginManager->createInstance($plugin, []);
    // Get search api's server entities.
    $query = \Drupal::entityQuery('search_api_server');
    // Get the search servers.
    $servers = $query->execute();
    // Loop through the search servers.
    foreach ($servers as $server_id) {
      $server_storage = $entityTypeManager->getStorage('search_api_server');
      $server = $server_storage->load($server_id);
      $backend = $server->getBackend();

      // Only export synoyms for Solr servers.
      if (is_a($backend, 'Drupal\search_api_solr\Plugin\search_api\backend\SearchApiSolrBackend')) {

        // Retrieve the Solr Connector which actually communicates
        // with the Solr server.
        $solr_connector = $backend->getSolrConnector();

        // Purge all synonyms from Solr.
        $instance->purgeSolrSynoymns($solr_connector);

        // Loop through each line of synonym mappings
        // and add to Solr.
        foreach ($lines as $line) {
          $path = 'schema/analysis/synonyms/english';
          $solr_connector->coreRestPost($path, $line);
        }

        // Reload Solr Core to activate synoymns.
        $solr_connector->reloadCore();

        // Que the index to re-index content.
        $indexes = $server->getIndexes();
        foreach ($indexes as $index) {
          $index->reindex();
        }
      }
    }
    $context['message'] = 'Export Synonyms to Solr....';
    $context['results'] = count($lines);
    usleep(50000);
  }

  /**
   * Static function import data finished.
   */
  public static function importDataFinished($success, $results, $operations) {
    $container = \Drupal::getContainer();
    $messenger = $container->get('messenger');
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
            $results,
            'One item processed.', '@count items processed.'
        );

    }
    else {
      $message = t('Finished with an error.');
    }
    $messenger->addStatus($message);
  }

}
