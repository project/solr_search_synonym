<?php

namespace Drupal\solr_search_synonym\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * To change this license header, choose License Headers in Project Properties.
 *
 * To change this template file, choose Tools | Templates.
 *
 * And open the template in the editor.
 */
class SolrSynonymUploaderTest extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Dependency injection of sevice.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_query
   *   The entity query.
   */

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityQuery;

  /**
   * Contruct function for declaring services.
   */
  public function __construct(EntityTypeManagerInterface $entity_query) {
    $this->entityQuery = $entity_query;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
    );
  }

  /**
   * Function index.
   */
  public function index() {

    $entityTypeManager = $this->entityQuery;
    $query = $entityTypeManager->get('solr_search_synonym')->getQuery();
    $query->accessCheck(FALSE);
    $synonyms = $query->execute();
    $syn_storage = $entityTypeManager->getStorage('solr_search_synonym');

    $lines = [];
    foreach ($synonyms as $synonym_id) {
      $synonym_entity = $syn_storage->load($synonym_id);
      $lines[] = '{' . $synonym_entity->getWord() . ':[' . $synonym_entity->getSynonyms() . ']}';
    }

    solr_search_synonym_cron();

    die('dc');

  }

}
