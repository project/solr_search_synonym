<?php

namespace Drupal\solr_search_synonym\Import;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Base class for solr search synonym import plugin managers.
 *
 * @ingroup plugin_api
 */
class ImportPluginManager extends DefaultPluginManager {

  /**
   * Active plugin id.
   *
   * @var string
   */
  protected $pluginId;

  /**
   * Import options.
   *
   * @var array
   */
  protected $importOptions;

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/solr_search_synonym/import', $namespaces, $module_handler, 'Drupal\solr_search_synonym\Import\ImportPluginInterface', 'Drupal\solr_search_synonym\Annotation\SearchSolrSynonymImport');
    $this->alterInfo('solr_search_synonym_import_info');
    $this->setCacheBackend($cache_backend, 'solr_search_synonym_import_info_plugins');
  }

  /**
   * Set active plugin.
   *
   * @param string $plugin_id
   *   The active plugin.
   */
  public function setPluginId($plugin_id) {
    $this->pluginId = $plugin_id;
  }

  /**
   * Get active plugin.
   *
   * @return string
   *   The active plugin.
   */
  public function getPluginId() {
    return $this->pluginId;
  }

  /**
   * Gets a list of available import plugins.
   *
   * @return array
   *   An array with the plugin names as keys and the descriptions as values.
   */
  public function getAvailableImportPlugins() {
    // Use plugin system to get list of available import plugins.
    $plugins = $this->getDefinitions();

    $output = [];
    foreach ($plugins as $id => $definition) {
      $output[$id] = $definition;
    }

    return $output;
  }

  /**
   * Validate that a specific import plugin exists.
   *
   * @param string $plugin
   *   The plugin machine name.
   *
   * @return bool
   *   TRUE if the plugin exists.
   */
  public function validatePlugin($plugin) {
    if ($this->getDefinition($plugin, FALSE)) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

}
