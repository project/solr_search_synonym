<?php

namespace Drupal\solr_search_synonym;

/**
 * ImportException extending Generic Plugin exception class.
 */
class ImportException extends \Exception {

}
