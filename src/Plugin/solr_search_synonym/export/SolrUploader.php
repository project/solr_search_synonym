<?php

namespace Drupal\solr_search_synonym\Plugin\solr_search_synonym\export;

use Drupal\solr_search_synonym\Export\ExportPluginBase;
use Drupal\solr_search_synonym\Export\ExportPluginInterface;

/**
 * Provides a synonym export and upload plugin for Apache Solr..
 *
 * @SearchSolrSynonymExport(
 *   id = "solr_uploader",
 *   label = @Translation("Solr with uploading"),
 *   description = @Translation("Synonym export and upload plugin for Apache Solr")
 * )
 */
class SolrUploader extends ExportPluginBase implements ExportPluginInterface {

  /**
   * Formats a word and its synonyms for sending to Solr.
   *
   * @param string $word
   *   Word variable.
   * @param string $synonyms
   *   Synonyms variable.
   *
   * @return string
   *   Returning string.
   */
  public function formatSynonymEntry($word, $synonyms) {
    $synonyms = explode(',', $synonyms);
    array_walk($synonyms, function (&$x) {
      $x = '"' . $x . '"';

    });
    $synonyms = implode(',', $synonyms);
    $word = '"' . $word . '"';
    return '{' . $word . ':[' . $synonyms . ']}';
  }

  /**
   * Retrieves the current synonyms from the Solr server.
   *
   * @param mixed $solr_connector
   *   Solr connector variable.
   *
   * @return array
   *   Returning string.
   */
  public function getCurrentSynonyms($solr_connector) {
    $path = 'schema/analysis/synonyms/english';
    // Get the synonyms.
    $response = $solr_connector->coreRestGet($path);

    $current_synonyms = [];

    if (0 === $response['responseHeader']['status']) {
      $current_synonyms = $response['synonymMappings']['managedMap'];
    }

    return $current_synonyms;
  }

  /**
   * Implements Drupal\solr_search_synonym\Export::getFormattedSynonyms.
   *
   * @param array $synonyms
   *   Synonyms variable.
   *
   * @return string
   *   Returning string.
   */
  public function getFormattedSynonyms(array $synonyms) {
    $lines = [];
    // Generate a line for each synonym.
    foreach ($synonyms as $synonym) {
      $lines[] = $this->generateLine($synonym->word, $synonym->synonyms, $synonym->type);
    }
    // If there is one or more synonymns in our array
    // Then send it to Solr.
    if (count($lines) > 0) {
      $this->exportToSolr($lines);
    }

    return implode("\n", $lines);
  }

  /**
   * Parses a Solr formatted synonym into a keyed array.
   *
   * @param string $line
   *   Line variable.
   *
   * @return array
   *   Returning array.
   */
  public function unformatSynonymEntry($line) {
    $search = ['"', '}', '{', '[', ']'];
    $replace = '';
    $line = str_replace($search, $replace, $line);
    $line = explode(':', $line);
    $array = [$line[0] => explode(',', $line[1])];
    return $array;
  }

  /**
   * Sends synoynms to Solr via Rest Api.
   *
   * @param array $lines
   *   Lines variable.
   */
  private function exportToSolr(array $lines) {
    // Get search api's server entities.
    $query = \Drupal::entityQuery('search_api_server');
    // Get the search servers.
    $servers = $query->execute();

    // Loop through the search servers.
    foreach ($servers as $server_id) {
      $server_storage = \Drupal::entityTypeManager()->getStorage('search_api_server');
      $server = $server_storage->load($server_id);
      $backend = $server->getBackend();

      // Only export synoyms for Solr servers.
      if (is_a($backend, 'Drupal\search_api_solr\Plugin\search_api\backend\SearchApiSolrBackend')) {

        // Retrieve the Solr Connector which actually communicates
        // with the Solr server.
        $solr_connector = $backend->getSolrConnector();

        // Purge all synonyms from Solr.
        $this->purgeSolrSynoymns($solr_connector);

        // Loop through each line of synonym mappings
        // and add to Solr.
        foreach ($lines as $line) {
          $path = 'schema/analysis/synonyms/english';
          $solr_connector->coreRestPost($path, $line);
        }

        // Reload Solr Core to activate synoymns.
        $solr_connector->reloadCore();

        // Que the index to re-index content.
        $indexes = $server->getIndexes();
        foreach ($indexes as $index) {
          $index->reindex();
        }
      }
    }
  }

  /**
   * Generate a single synonyms line for the export file.
   *
   * @param string $word
   *   The main word.
   * @param string $synonyms
   *   The comma separated string with synonyms.
   * @param string $type
   *   Synonym (synonym) og Spelling error (spelling_error)
   *
   * @return string
   *   Return the single line with synonyms and the corresponding word.
   */
  public function generateLine($word, $synonyms, $type) {

    $line = '';

    switch ($type) {
      case 'synonym':
        // We force using of equivalent mappings for type = synonym.
        $line = $this->formatSynonymEntry($word, $synonyms);
        break;

      case 'spelling_error':
        // @todo Add capability to support Spelling Errors in needed
        $line = $this->formatSynonymEntry($word, $synonyms);
        break;
    }
    return $line;
  }

  /**
   * Removes all managed Solr Synonyms from Solr server.
   *
   * @param mixed $solr_connector
   *   Solr connector parameter.
   */
  public function purgeSolrSynoymns($solr_connector) {
    $client = \Drupal::httpClient();
    $current_synonyms = $this->getCurrentSynonyms($solr_connector);
    $endpoint = $solr_connector->getEndpoint();

    foreach ($current_synonyms as $key => $synonyms) {
      $path = 'schema/analysis/synonyms/english/' . $key;
      $uri = $endpoint->getBaseUri() . $path;
      $client->request('DELETE', $uri);
    }
  }

}
