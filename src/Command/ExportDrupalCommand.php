<?php

namespace Drupal\solr_search_synonym\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Drupal\Console\Core\Command\Shared\ContainerAwareCommandTrait;
use Drupal\Console\Core\Style\DrupalStyle;

/**
 * Drupal Console Command for export synonyms.
 *
 * @package Drupal\solr_search_synonym
 *
 * @DrupalCommand (
 *     extension="solr_search_synonym",
 *     extensionType="module"
 * )
 */
class ExportDrupalCommand extends Command {

  use ContainerAwareCommandTrait;

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('searchsolr:synonym:export')
      ->setDescription($this->trans('commands.searchsolr.synonym.export.description'))
      ->addOption(
        'plugin',
        NULL,
        InputOption::VALUE_REQUIRED,
        $this->trans('commands.searchsolr.synonym.export.options.plugin.description')
      )
      ->addOption(
        'langcode',
        NULL,
        InputOption::VALUE_REQUIRED,
        $this->trans('commands.searchsolr.synonym.export.options.langcode.description')
      )
      ->addOption(
        'type',
        NULL,
        InputOption::VALUE_OPTIONAL,
        $this->trans('commands.searchsolr.synonym.export.options.type.description'),
        'all'
      )
      ->addOption(
        'filter',
        NULL,
        InputOption::VALUE_OPTIONAL,
        $this->trans('commands.searchsolr.synonym.export.options.filter.description'),
        'all'
      )
      ->addOption(
        'incremental',
        NULL,
        InputOption::VALUE_OPTIONAL,
        $this->trans('commands.searchsolr.synonym.export.options.incremental.description')
      )
      ->addOption(
        'file',
        NULL,
        InputOption::VALUE_OPTIONAL,
        $this->trans('commands.searchsolr.synonym.export.options.file.description')
      );
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    // Plugin manager.
    $pluginManager = \Drupal::service('plugin.manager.solr_search_synonym.export');

    // Options.
    $plugin = $input->getOption('plugin');
    $langcode = $input->getOption('langcode');
    $type = $input->getOption('type');
    $filter = $input->getOption('filter');
    $file = $input->getOption('file');
    $incremental = $input->getOption('incremental');

    // Command output.
    $io = new DrupalStyle($input, $output);

    // Validate option: plugin.
    if (!$pluginManager->validatePlugin($plugin)) {
      $error = TRUE;
      $io->info($this->trans('commands.searchsolr.synonym.export.messages.invalidplugin'));
    }

    // Validate option: langcode.
    if (empty($langcode)) {
      $error = TRUE;
      $io->info($this->trans('commands.searchsolr.synonym.export.messages.invalidlangcode'));
    }

    // Validate option: type.
    if (!empty($type) && !$this->validateOptionType($type)) {
      $error = TRUE;
      $io->info($this->trans('commands.searchsolr.synonym.export.messages.invalidtype'));
    }

    // Validate option: filter.
    if (!empty($filter) && !$this->validateOptionFilter($filter)) {
      $error = TRUE;
      $io->info($this->trans('commands.searchsolr.synonym.export.messages.invalidfilter'));
    }

    // Prepare export.
    if (!isset($error)) {
      $io->info($this->trans('commands.searchsolr.synonym.export.messages.start'));

      $options = [
        'langcode' => $langcode,
        'type' => $type,
        'filter' => $filter,
        'file' => $file,
        'incremental' => (int) $incremental,
      ];
      $pluginManager->setPluginId($plugin);
      $pluginManager->setExportOptions($options);

      // Execute export.
      if ($result = $pluginManager->executeExport()) {

        // Output result.
        $io->info($this->trans('commands.searchsolr.synonym.export.messages.success'));
        $io->info($result);
      }
    }
  }

  /**
   * Validate that the type option is valid.
   *
   * @param string $type
   *   Type value from --type command option.
   *
   * @return bool
   *   TRUE if valid, FALSE if invalid.
   */
  private function validateOptionType($type) {
    $types = ['synonym', 'spelling_error', 'all'];
    return in_array($type, $types);
  }

  /**
   * Validate that the filter option is valid.
   *
   * @param string $filter
   *   Type value from --filter command option.
   *
   * @return bool
   *   TRUE if valid, FALSE if invalid.
   */
  private function validateOptionFilter($filter) {
    $filters = ['nospace', 'onlyspace', 'all'];
    return in_array($filter, $filters);
  }

}
