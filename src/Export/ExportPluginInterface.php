<?php

namespace Drupal\solr_search_synonym\Export;

use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;

/**
 * Provides an interface for solr search synonym export plugins.
 *
 * @ingroup plugin_api
 */
interface ExportPluginInterface extends PluginFormInterface, ConfigurableInterface, DependentPluginInterface {

  /**
   * Get synonyms in the export format.
   *
   * @param array $synonyms
   *   An array containing synonym objects.
   *
   * @return string
   *   The formatted synonyms as a string ready to be saved to an export file.
   */
  public function getFormattedSynonyms(array $synonyms);

}
