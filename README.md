CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Manage synonyms
 * Export synonyms
 * Developers
 * Troubleshooting
 * Sponsors
 * Maintainers

INTRODUCTION
------------
This module let editors or administrators manage synonyms for SOLR Search
directly in Drupal.

Synonyms can be export using the build in Drupal Console command.
Drush command and automatic export using Drupal cron job is in
development.

The module support the synonyms.txt format used in Apache Solr.
Other formats can be added using the Export plugin annotation.

This module is a extended version of Search API Synonym
(search_api_synonym) module. It provides integration with Apache
Solr and export synonyms directly in solr configuration.

REQUIREMENTS
------------
* No requirements.

INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
  https://www.drupal.org/documentation/install/modules-themes/modules-8
  for further information.

MANAGE SYNONYMS
---------------
After installation can you start managing your synonyms and spelling errors
at admin/config/search/solr-search-synonyms.

EXPORT SYNONYMS
---------------

Drupal Console
--------------

Export the added synonyms using the Drupal Console command:

- drupal searchsolr:synonym:export

Execute the command with --help to see the different options.

Drush 8 and previous versions
-----------------------------

Export synonyms using the Drush command:

- drush solr-search-synonym-export

- drush ssolr-syn-ex

Drush 9
-------

Export synonyms using the Drush command:

- drush solr-search-synonym:export

- drush ssolr-syn:export

- drush ssolr-syn-ex

Examples
--------
Replace [COMMAND] with one of the above Drupal Console / Drush commands.

Export all English synonyms and spelling errors in the Solr format.

- [COMMAND] --plugin=solr --langcode=en

Export all English spelling errors in the Solr format.

- [COMMAND] --plugin=solr --langcode=en --type=spelling_error

Export all English synonyms without white spaces in the Solr format.

- [COMMAND] --plugin=solr --langcode=en --type=synonym --filter=nospace

For exporting synonyms directly into the solr -

Update SynonymGraphFilterFactory class setting with
ManagedSynonymGraphFilterFactory with managed attribute in /solr/
data/{index_name}/conf/schema_extra_types.xml file.

For Example - 
1. Configure the SOLR backend server and index
on admin/config/search/search-api.
2. Update <filter class="solr.SynonymGraphFilterFactory"
synonyms="synonyms_und.txt" ignoreCase="true"/> with <filter
class="solr.ManagedSynonymGraphFilterFactory" managed="english"/>
for fieldType name="text_und" in /solr/data/{index_name}/conf/
schema_extra_types.xml file. This configuration worked with 8.9.0
solr version. For other versions and field types, configuration
may be differ.
3. Restart SOLR.
4. Execute below command -

- [COMMAND] --plugin=solr_uploader --langcode=en
5. Verify Synonyms with current site search.

Cron
----

Export using Drupal cron is supported. See the settings in /admin/
config/search/solr-search-synonyms/settings.

For exporting synonyms directly into the solr make sure to update
schema_extra_types.xml file first and restart solr. then while
cron executed, the synonyms will be exported succussfully in solr.

DEVELOPERS
----------

The Solr Search Synonym module provides the following ways for developers to
extend the functionality:

- Plugins
  Export plugin - see the annotation and the Solr plugin:
  - Drupal\solr_search_synonym\Annotation\SearchSolrSynonymExport
  - Drupal\solr_search_synonym\Plugin\solr_search_synonym\export\Solr
  - Drupal\solr_search_synonym\Plugin\solr_search_synonym\export\SolrUploader

TROUBLESHOOTING
---------------
-


MAINTAINERS
-----------
Current maintainers:
 * Pankaj Bhojwani (bhojwanipankaj05) - https://www.drupal.org/u/bhojwanipankaj05
